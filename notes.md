# Notes for *Java A Beginner's Guide*
* ```static``` method can be use  with both the class or an instance of the class
* ```for (;;)``` gives an infinite for loop
* ```?``` ternary condition operator
```java
x = condition ? value1 : value2;
//is equivalent of
if (condition true) {x = value1;}
else {x = value2}
```
* ```break``` label break the block with the label and execute anything after that labelled block
  - which could be use jump out of nests of loops (except for single one) if the nested loop is in a labelled block
* arguments of variable lengths must be declare at last and each function could only have one such argument
* overloading with argument with variable lengths must have different types of arguments
```java
class MyClass{
    myFunc (int ... v){
      // access input int with v[index]
    }
    myFunc (int ... v, String s){
      // invalid argument declaration
    }
    myFunc (int a, int b, int ... v){
      //this is an invalid overloading
    }
}
```
* a ```final``` method can not be override
* a ```final``` class can not be inherit
* a ```final``` variable will not be changed by any method or re-assigned new value
* the ```Object``` class is the superclass of every class with lots of pre-defined methods, such as printing:```toString```
* ```String toString()``` is the printing equivalent magic method like python for an object, which will override the default printing method in ```Object``` superclass if specified
* default visibility of a member of a class is at package level
* all actual implementation of methods in interface in a class must be public as that's how method in an interface is set by default
* abstract class can not instantiate any instance [make sense with the name of abstract there's no actual existence of its presence]
* all interface variables are ```public```, ```static``` and ```final```
* interface can inherit each others with ```extends``` as well but class that ```implement``` a child interface must implement all methods defined in the family tree
* interface methods prioritisation:
  - first: any override method defined in class
  - if a class implement two interfaces each with a ```default``` method with same name and the class does not override that method, error will come out
  - again with the same-name ```default``` methods but two interface is parent-child relation, the default method in child interface will be implement
* ```static``` interface methods are not inherited by either an implementing class or a child-interface
* ```static``` interface methods can only be called with the interface due to above reason while static method in class can be called with both an instance or the class itself
- Chapter 9: Exception
  * All exception are derived from ```Throwable```
    - Hierarchy is that ```Exception``` and ```Error``` are direct subclass of Throwable but ```Error``` is out of our control
    - ```RuntimeException``` is a subclass of exception
    - subclass of ```RuntimeException``` or ```Error``` do not need to be specifically ```throws``` in calling method, known as unchecked exceptions
  * exception thrown out of a method is specified by ```throws```
  * ```throw``` is for manually exception throwing
  * lines that must be executed after ```try, catch``` is put in ```finally```
  * multiple catch is allowed with ```catch(ExceptionType1 | ExceptionType2 exc)``` or chained single catch statement
- Chapter 10: IO
  * common approach is character-based streams to read from console or file
  ```Java
  BufferedReader myReader = new BufferedReader(new FileReader(new File(filePathnName)));
  //or
  BufferedReader myReader = new BufferedReader(new InputStreamReader(System.in))
  // where InputStreamReader convert byte stream to character stream
  //then
  myReader.readLine() //will be an iterator and will be null at the end of file
  myReader.readLine() //return a String[], so can apply split(delimiter)
  //be cautious to initialise it before applying it as while loop condition
  //but each time it is used will invoke a jumping on reading line pointer in document, so be careful for skipping lines in process
  ```
- Chapter 11: Multithreading
  * ```new Thread(Runnable object)``` is one of the constructor of Thread, where object is any class that implement Runnable
  * ```new Thread(Runnable object,String name)``` is another, where name will be caught by ```getName()```
  * difference between ```implements Runnable``` and ```extends Thread``` is that:
    - ```extends Thread``` allow one to override some other methods defined in Thread, though only the override of ```run()``` is compulsory
    - so if one do not need to achieve the above mentioned functionality then just ```implements Runnable``` is sufficient
    - also ```implements Runnable``` allow one to ```extends``` some other class
    - actually one class can ```implements Runnable``` and ```extends Thread``` at the same time
    - instantiation differs:
    ```java
    class MyThread1 extends Thread {
        MyThread1(String){
          super(name);
        }
    }
    class MyThread2 implements Runnable{
        String name;
        MyThread2(Sting name){
          this.name = name;//just as traditional constructor
        }
    }
    //or
    class MyThread2_1 implements Runnable{
        Thread thread;
        String name;
        MyThread2_1(String name){
          this.name = name;
          this.thread = new Thread(this, name);
        }
    }
    class Main{
        public static void main(String[] args){
          MyThread1 myThread1 = new MyThread1("threadName");
          MyThread2_1 myThread2_1 = new MyThread2_1("threadName");
          Thread myThread2 = new Thread(new MyThread2("threadName"));
        }
    }
    ```
  * ```Synchronized``` lock an object in current thread until current thread exit and protect it from being accessed by other threads
  * ```Thread.sleep(int)``` is used to allow switch of task, while current thread sleep other thread can run, but sleep inside sync block will never achieve switch functionality as current block is locked and other can not take place
  * ```wait()```, ```notify()```, ```notifyAll()``` should only be called in a sync block
  * when ```wait()``` is called in a sync block, that thread surrender its control and sleep and will only resume when some other sync block call ```notify()``` or ```notifyAll()```
- Chapter 12: Enum, auto-boxing, static import and  annotation
  *
- Chapter 13: Generics
  * allow type to be pass as an input parameter to a class/interface, declare right after the class name eg: ```MyClass<Generics>```
  * similarly for method (function) inside class but need to be declare before the output type of method eg:```public static <T> void myFunction(T input)```
  * also work for constructor even if the class is not initialise with generics, similar to method, but constructor don't have return type so generics are declared before constructor (name)
  * ```<T extends SuperClass>``` allow the T inside class to use super-class-type specific methods
  * ```?``` is the wildcard argument when referring to an object using generics type, seen in the override equal and hash method, eg:
  ```Java
  public class GenClass<T>{
    public void compare(GenClass<?> obj){
      //this function would take any generic implementation of GenClass as input
    }
  }
  ```
  * any class that implement a generic interface must be generic itself unless the generics has been specified already when call with implementing.
  * raw type is an instance of a generic class that did not specify type upon creation, which is allowed as type is taking the default universal Object type but may cause future problems
  * static member declare by the type parameter is not allowed, so as a static method that return a member of type in generic
  * Generic type construction is not allowed:
  ```Java
  T ob = new T(); //is wrong!
  ```
  * Array instantiate with a generic type is not allowed, eg:
  ```Java
  T vals[]; //is ok
  vals = new T[10]; //is wrong
  ```
  * Array of instances of generic class is also not allow, eg:
  ```Java
  public class MyGenClass<T>{
    //code
  }
  public class Demo{
    public static void main(String[] args){
      MyGenClass<Integer>[] genClassArray = new MyGenClass<Integer>[10]; // is not allowed
    }
  }
  ```
  * a generic class can not extends Throwable, which means can not create generic exception class
- Chapter 14: Lambda expression and method reference
  * functional interface and lambda expression example:
  ```Java
  public interface MyFunctionalInterface{
      int getValue(n); //this method is default abstract as it is non-static non-default
  }
  public class Demo{
      public static void main(String[] args){
        MyFunctionalInterface myTest;
        myTest = (n) -> n; //lambda expression pass to only abstract method in functional interface
        // but the output and input arguments of lambda expression and abstract method should match
        System.out.println(myTest.getValue(10)) // will print 10
      }
  }
  ```
  * so multiple functions(methods) can be created just through an functional interface as long as they have matching input and output type
  * if such functional interface is generic, that there would be large flexibility
  * block lambda expression must explicitly provide a return:
  ```Java
  public interface MyFunctionalInterface{
      int manipulate(n); //this method is default abstract as it is non-static non-default
  }
  public class Demo{
      public static void main(String[] args){
        MyFunctionalInterface myTest = (n) -> {
          int sum = 0;
          for (int i = 0; i < n, i++) sum += i;
        }
        return sum;
      }
  }
  ```
  * if the functional interface is generic, it can still use a lambda expression and the type is specified upon the declaration of the functional interface
  * lambda expression can use variable outside its block, but can not modified them
  * method reference with generic method:
  ```Java
  interface MyFunctionalInterface<T>{
    boolean test(T input);
  }
  class MyClass{
    static <T> boolean myGenericMethod(T input){
      return false;
    }
  }
  // the reference:
  MyFunctionalInterface<Integer> methodRef = MyClass::<Integer>myGenericMethod;
  ```
  * constructor reference: ```MyClass::new```
  * several in-built functional interface and its abstract method name:
    - ```Predicate<T>::test``` return boolean
    - ```Function<T,R>::apply``` return R
