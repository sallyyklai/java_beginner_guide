public class BlockLambdaForInterfaceDemo {
  public static void main(String[] args){
    MyFunctionalInterface myTestCase = (n) -> {
      int sum = 0;
      for (int i = 0; i <= n; i++){
        sum += i;
      }
      return sum;
    };
    System.out.println(myTestCase.getValue(3));
  }
}
