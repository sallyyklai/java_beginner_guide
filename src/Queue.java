public class Queue {
    char q[];
    int putloc, getloc;
    public Queue(int size){
        q = new char[size];
        putloc = getloc = 0;
    }
    void put(char ch) {
        if (putloc==q.length) {
            System.out.println("Queue is full");
            return;
        }
        q[putloc++] = ch;
    }
    char get() {
        if (putloc == getloc) {
            System.out.println("Queue is empty");
            return (char) 0;
        }
        return q[getloc++];
    }
    public static void main(String[] args){
        char smallQ[] = {'A','B','C'};
//        for (int i = 0; i < 4; i++) {
//            smallQ[i] = (char) ('Z' - i);
//        }
        for (char x: smallQ) {
            System.out.println(x);
        }
    }
}
