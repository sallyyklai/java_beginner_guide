public class PriorityThread implements Runnable {
  int count;
  Thread thread;
  static boolean stop = false;
  static String currentName;

  public PriorityThread(String name) {
    this.thread = new Thread(this,name);
    this.count = 0;
    this.currentName = name;
  }

  @Override
  public void run() {
    System.out.println(thread.getName() + " starting");
    do {
      count++;
      if (currentName.compareTo(thread.getName()) != 0){
        currentName = thread.getName();
        System.out.println("In "+ currentName + " with count: " + count);
      }
    } while (stop == false && count < 100);
    stop = true;
    System.out.println("\n" + thread.getName() + " terminating");
  }
}

class PriorityDemo {
  public static void main(String[] args){
    PriorityThread t1 = new PriorityThread("Higher priority thread");
    PriorityThread t2 = new PriorityThread("Lower priority thread");
    t1.thread.setPriority(7);
    t2.thread.setPriority(3);
    t1.thread.start();
    t2.thread.start();
    try {
      t1.thread.join();
      t2.thread.join();
    } catch (InterruptedException e) {
      System.out.println("main thread interrupted");
    }
    System.out.println("\n Higher priority thread count to: " + t1.count);
    System.out.println("\n Lower priority thread count to: " + t2.count);
  }
}
