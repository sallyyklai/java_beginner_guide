interface IntWithDefault{
    int getUserID();
    default int getAdminID(){return 1;}
    static int getUniversalID() {return 0;}
}
class ClassWithInt implements IntWithDefault {
    public int getUserID() {
        return 1118;
    }
    public int getAdminID(){return 1000;}
}
public class DefaultInterfaceDemo {
    public static void main(String[] args){
        ClassWithInt newObj = new ClassWithInt();
        System.out.println(newObj.getUserID());
        System.out.println(newObj.getAdminID());
        System.out.println(IntWithDefault.getUniversalID()); // that's how static method in interface is called
//        System.out.println(newObj.getUniversalID());
    }
}
