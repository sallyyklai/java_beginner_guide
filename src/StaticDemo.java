public class StaticDemo {
    String demoName;
    public StaticDemo(String demoName) {
        this.demoName = demoName;
    }
    public void anotherMethod() {
        System.out.println("hello from another");
    }
    public static void tryStatic () {
        System.out.println("hello from static");
    }
    public static void main(String[] args) {
        StaticDemo demo1 = new StaticDemo("firstdemo");
        demo1.anotherMethod();
        StaticDemo.tryStatic();
        demo1.tryStatic();
//        StaticDemo.anotherMethod(); this will not be allowed as anotherMethod is not static
    }
}
