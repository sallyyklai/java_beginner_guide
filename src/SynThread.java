public class SynThread implements Runnable {
  Thread thread;
  static SumArray sumArray = new SumArray();
  int nums[];
  int answer;

  public SynThread(String name, int[] nums) {
    thread = new Thread(this,name);
    this.nums = nums;
    thread.start();
  }

  @Override
  public void run() {
    int sum;
    System.out.println(thread.getName() + " starting");
    answer = sumArray.sumArray(nums);
    /*
    * another method would be: not declaring sync in the below sumArray
    * but declared it here:
    * symchronized (sa){ answer = sa.sumArray(nums)}
    */
    System.out.println("Sum for " + thread.getName() + " is " + answer);
    System.out.println(thread.getName() + " terminating");
  }
}

class SumArray{
  private int sum;
  synchronized int sumArray(int nums[]){
    sum = 0;
    for (int num:nums){
      sum += num;
      System.out.println("running total for "+ Thread.currentThread().getName() + " is " + sum);
    }
    try {
      Thread.sleep(10);
    } catch (InterruptedException e) {
      System.out.println("interrupted");
    }
    return sum;
  }
}

class Sync{
  public static void main(String[] args){
    int nums[] = {1,2,3,4,5};
    SynThread t1 = new SynThread("child 1", nums);
    SynThread t2 = new SynThread("child 2", nums);
    try {
      t1.thread.join();
      t2.thread.join();
    } catch (InterruptedException e) {
      System.out.println("interrupted");
    }
  }
}
