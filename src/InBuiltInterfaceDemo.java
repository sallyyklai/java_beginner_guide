import java.util.function.Predicate;
import java.util.function.Function;
public class InBuiltInterfaceDemo {
  public static void main(String[] args){
    Predicate<Integer> isEven = (n) -> (n % 2) == 0;
    System.out.println(isEven.test(4));
    Function<Integer,Boolean> isOdd = (n) -> (n % 2) != 0;
    System.out.println(isOdd.apply(3));
  }
}
