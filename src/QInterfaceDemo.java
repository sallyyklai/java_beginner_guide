class FixedQ implements QInterface {
    private char q[];
    private int putloc, getloc;
    public FixedQ(int size){
        q = new char[size];
        putloc = getloc = 0;
    }

    @Override
    public void put(char ch) {
        if(putloc==q.length) {
            System.out.println(" - Queue is full.");
            return;
        }
        q[putloc++] = ch;
    }

    @Override
    public char get() {
        if(getloc == putloc) {
            System.out.println(" - Queue is empty.");
            return (char) 0;
        }
        return q[getloc++];
    }
}
class CircularQ implements QInterface{
    private char q[];
    private int putloc, getloc;
    public CircularQ(int size) {
        q = new char[size+1]; // allocate memory for queue
        putloc = getloc = 0;
    }

    @Override
    public void put(char ch) {
        if(putloc+1==getloc | ((putloc==q.length-1) & (getloc==0))) {
            System.out.println(" - Queue is full.");
            return;
        }
        q[putloc++] = ch;
        if(putloc==q.length) putloc = 0; // loop back
    }

    @Override
    public char get() {
        if(getloc == putloc) {
            System.out.println(" - Queue is empty.");
            return (char) 0;
        }
        char ch = q[getloc++];
        if(getloc==q.length) getloc = 0; // loop back
        return ch;
    }
}
public class QInterfaceDemo {
    public static void main(String[] args){
        FixedQ q1 = new FixedQ(10);
        CircularQ q3 = new CircularQ(10);
        char ch;
        int i;
        for(i=0; i < 10; i++) {
            q1.put((char) ('A' + i));
            q3.put((char) ('A' + i));
        }
        System.out.print("Contents of fixed queue: ");
        for(i=0; i < 10; i++) {
            ch = q1.get();
            System.out.print(ch);
        }
        System.out.println();System.out.print("Contents of circular queue: ");
        for(i=0; i < 20; i++) {
            ch = q3.get();
            System.out.print(ch);
        }
        System.out.println();
    }
}
