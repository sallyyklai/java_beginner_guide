public class ThrowableFunctionDemo {
  public static void genException(){
    int nums[] = new int[4];
    nums[7] = 10;
  }
  public static void main(String[] args){
    try {
      genException();
    } catch (ArrayIndexOutOfBoundsException exc){
      System.out.println("General print:");
      System.out.println(exc);
      System.out.println("Detail print:");
      exc.printStackTrace();
    }
  }
}
