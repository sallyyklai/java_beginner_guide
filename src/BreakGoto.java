public class BreakGoto {
    public static void main(String[] args){
        outer: for (int i = 0; i <= 10; i ++) {
            System.out.println("Outer i is " + i);
            System.out.print("inner j is: " );
            inner : for (int j = 0; j <= 5; j++) {
                System.out.print(j + " ");
                System.out.println();
                if (j == 3) break inner;
            }
            System.out.println("inner is break");

        }
    }
}

