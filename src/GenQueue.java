public class GenQueue<T> implements GeneiricsQInterface<T> {
  private T q[];
  private int putloc, getloc;

  public GenQueue(T[] q) {
    this.q = q;
    putloc = getloc = 0;
  }

  @Override
  public void put(T ch) throws QFullEx {
    if (putloc == q.length) throw new QFullEx(q.length);
    q[putloc++] = ch;
  }

  @Override
  public T get() throws QEmptyEx {
    if (getloc == putloc) throw new QEmptyEx();
    return q[getloc++];
  }
}

class GenQDemo {
  public static void main(String[] args){
    Integer iStore[] = new Integer[3];
    GenQueue<Integer> q = new GenQueue<>(iStore);
    for (int i = 0; i < 5; i++){
      try{
        q.put(i);
        System.out.println("Adding " + i + " to q.");
      } catch (QFullEx exc) {
        System.out.println(exc);
      }
    }
    for (int j = 0; j < 5; j++){
      try{
        int value = q.get();
        System.out.println("Getting from q: " + value);
      } catch (QEmptyEx exc) {
        System.out.println(exc);
      }
    }
  }
}
