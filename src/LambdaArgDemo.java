public class LambdaArgDemo {
  public static String changeStr(StringFunc sf, String s){
     return sf.changeStr(s);
  }
  public static void main(String[] args){
//    StringFunc reverse = (s) -> {
//      String result = "";
//      for (int i = s.length()-1; i >=0; i--){
//        result += s.charAt(i);
//      }
//      return result;
//    };
//    StringFunc replace = (s) -> s.toUpperCase();
    String inputString = "Hello World";
//    String outputString = changeStr((s) -> {
//      String result = "";
//      for (int i = s.length()-1; i >=0; i--){
//        result += s.charAt(i);
//      }
//      return result;
//    },inputString);
    String outputString = changeStr((s) -> s.toUpperCase(),inputString);
    System.out.println(outputString);
  }
}
