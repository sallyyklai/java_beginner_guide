import java.util.Objects;

public class Generics<T,S> {
  T content;
  S side;

  public Generics(T content, S side) {
    this.content = content;
    this.side = side;
  }

  public T getContent() {
    return content;
  }

  public S getSide() {
    return side;
  }

  public void setContent(T content) {
    this.content = content;
  }

  public void setSide(S side) {
    this.side = side;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Generics<?, ?> generics = (Generics<?, ?>) o;
    return Objects.equals(content, generics.content) &&
        Objects.equals(side, generics.side);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content, side);
  }
}

//class Demo{
//  public static void main(String[] args){
//    Generics<Integer,String> myInt = new Generics<>(18,"Good day");
//    Generics<String,Integer> myString = new Generics<>("Hello World",22);
//    System.out.println(myInt.getContent().getClass());
//    System.out.println(myInt.getSide().getClass());
//    System.out.println(myString.getContent().getClass());
//    System.out.println(myString.getSide().getClass());
//  }
//}

class RawDemo{
  public static void main(String[] args){
    Generics myRaw = new Generics(8, "Hello string"); // did not specify generic type
    System.out.println(myRaw.getContent().getClass());
    System.out.println(myRaw.getSide().getClass());
  }
}
