public class QEmptyEx extends Exception {
  @Override
  public String toString() {
    return "\nQueue is empty";
  }
}
