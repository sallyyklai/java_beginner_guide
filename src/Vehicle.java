public class Vehicle {
    int passengers;
    int fuelcap;
    int mpg;

    public Vehicle(int passengers, int fuelcap, int mpg) {
        this.passengers = passengers;
        this.fuelcap = fuelcap;
        this.mpg = mpg;
    }
    double fuelneeded(int miles) {
        return (double) miles / mpg;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public int getFuelcap() {
        return fuelcap;
    }

    public void setFuelcap(int fuelcap) {
        this.fuelcap = fuelcap;
    }

    public int getMpg() {
        return mpg;
    }

    public void setMpg(int mpg) {
        this.mpg = mpg;
    }
    public static void main(String[] args){
        Vehicle vehicle = new Vehicle(60, 16, 21);
        System.out.println(vehicle.getMpg());
        System.out.println(vehicle.getFuelcap());
        System.out.println(vehicle.getPassengers());
    }
}
