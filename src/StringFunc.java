@FunctionalInterface
public interface StringFunc {
  String changeStr(String str);
  static String addExclamation(String str){
    return str + "!";
  }
  static String addQuestion(String str){
    return str + "?";
  }
}
