import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class TestHttp {
  public static void main(String[] args){
    String orderNumber = "4564564";
//    System.out.println(String.format("http://gb2-st-oms-001.io.thehut.local:8080/OrderManagementSystem/audit/add?orderNumber=%s}&user=test&action=auditproduct&application=(application)",orderNumber));
    try {
      URL url = new URL(String.format("http://gb2-st-oms-001.io.thehut.local:8080/OrderManagementSystem/audit/add?orderNumber=%s&user=test&action=auditproduct&application=(application)",orderNumber));
      System.out.println(connect(url));
    } catch (Exception e) {
      System.out.println("buugy");
    }
  }

  public static int connect(URL url) throws IOException {
    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
    connection.setRequestMethod("POST");
    connection.setConnectTimeout(10000);
    connection.connect();
    int responseCode = connection.getResponseCode();
    System.out.println(connection.getResponseMessage());
    connection.disconnect();
    return responseCode;
  }
}
