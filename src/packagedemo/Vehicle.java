package packagedemo;

public class Vehicle {
    int passengers;
    int fuelcap;
    int mpg;
    String name;

    Vehicle(int passengers, int fuelcap, int mpg, String name) {
        this.passengers = passengers;
        this.fuelcap = fuelcap;
        this.mpg = mpg;
        this.name = name;
    }
}
