public class MethodReferenceDemo {
  public static String changeStr(StringFunc sf, String s){
    return sf.changeStr(s);
  }
  public static void main(String[] args){
    String output;
//    output = changeStr(StringFunc::addExclamation,"Hello World");
    output = changeStr(StringFunc::addQuestion,"How are you");
    System.out.println(output);
  }
}
