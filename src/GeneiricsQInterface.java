public interface GeneiricsQInterface<T> {
  void put(T ch) throws QFullEx;
  T get() throws QEmptyEx;
}
