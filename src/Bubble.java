public class Bubble {
    public static void main(String[] args) {
        int numb[] = {99, -10, 100123, 18, -978, 5623, 463, -9, 287, 49};
        int a, b, t;
        for (a = 1; a < numb.length; a++){
            for (b = numb.length - 1; b >= a; b--) {
                if (numb[b-1] > numb[b]) {
                    t = numb[b-1];
                    numb[b-1] = numb[b];
                    numb[b] = t;
                }
            }
        }
        System.out.print("Sorted: ");
        for (int i = 0; i < numb.length; i++) {
            System.out.print(numb[i] + " ");
        }
        System.out.println();
    }
}
