public class Queue2 {
    private char q[];
    private int putloc, getloc;
    Queue2(int size){
        q = new char[size];
        putloc = getloc = 0;
    }
    void put(char ch) {
        if (putloc==q.length) {
            System.out.println("Queue is full");
            return;
        }
        q[putloc++] = ch;
    }
    char get() {
        if (putloc == getloc) {
            System.out.println("Queue is empty");
            return (char) 0;
        }
        return q[getloc++];
    }
}
