public class MyThread extends Thread {
  public MyThread(String name) {
    super(name);
    start();
  }
  public void run(){
    System.out.println(getName() + " starting");
    try{
      for (int counter = 0; counter < 10; counter++){
        Thread.sleep(400);
        System.out.println("In "+ getName() + " the counter is: " + counter);
      }
    } catch (InterruptedException e) {
      System.out.println(getName() + " is interrupted");
    }
    System.out.println(getName() + " terminating");
  }
}

class UseThreadsImproved {
  // Main thread and ThreadDemo will run concurrently
  public static void main(String[] args) {
    System.out.println("main thread starting");
    MyThread myThread1 = new MyThread("child1"); // now run upon creation
    MyThread myThread2 = new MyThread("child2");
    MyThread myThread3 = new MyThread("child3");
    do {
      System.out.print(".");
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        System.out.println("interrupted");
      }
    } while (myThread1.isAlive() || myThread2.isAlive() || myThread3.isAlive()); // now main end asa three other threads end
    System.out.println("main thread end");
  }
}
