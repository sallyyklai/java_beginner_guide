class X {
    int a;

    public X(int a) {
        this.a = a;
    }
}

class Y extends X {
    int a,b;

    public Y(int a, int b) {
        super(a);
        this.b = b;
    }
}

public class SupSubRef {
    public int x = 1009;
    public static void main(String[] args){
        X x1 = new X(1);
        X x2 = new X(6);
        Y y1 = new Y(2,3);
        x2 = y1; //an instance of a superclass can be assigned to an instance of its subclass, but will not have the extra things that subclass have
        System.out.println(x2.a);
//        System.out.println(x2.b); not working as X dont have a b property
    }
}
