class ThreadJoinDemo{
  public static void main(String[] args){
    System.out.println("main thread starting");
    MyThread myThread1 = new MyThread("child1"); // now run upon creation
    MyThread myThread2 = new MyThread("child2");
    MyThread myThread3 = new MyThread("child3");
    try {
      myThread1.join();
      System.out.println(myThread1.getName() + " join");
      myThread2.join();
      System.out.println(myThread2.getName() + " join");
      myThread3.join();
      System.out.println(myThread3.getName() + " join");
    } catch (InterruptedException e) {
      System.out.println("main thread interrupted");
    }
    System.out.println("main thread terminating");
  }
}
