public class ThreadDemo implements Runnable {
  String threadName;

  public ThreadDemo(String threadName) {
    this.threadName = threadName;
  }

  @Override
  public void run() {
    System.out.println(threadName + "starting");
    try{
      for (int counter = 0; counter < 10; counter++){
        Thread.sleep(400);
        System.out.println("The counter is: " + counter);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}

class UseThreads{
  // Main thread and ThreadDemo will run concurrently
  public static void main(String[] args){
    System.out.println("main thread starting");
    Thread newThread = new Thread(new ThreadDemo("child1"));
    newThread.start();
    for (int i = 0; i <50; i++){
      System.out.print(".");
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        System.out.println("interrupted");
      }
    }
    System.out.println("main thread end");
  }
}
