public class TickTockWaitNotify {
  String state;
  synchronized void tick(boolean running){
    if(!running){
      state = "ticked";
      notify();
      return;
    }
    System.out.print("Tick ");
    state = "ticked";
    notify();
    try{
      while(!state.equals("tocked")){
        wait();
      }
    } catch (InterruptedException exc){
      System.out.println("Thread interrupted");
    }
  }
  synchronized void tock(boolean running){
    if(!running){
      state = "tocked";
      notify();
      return;
    }
    System.out.println("Tock");
    state = "tocked";
    notify();
    try{
      while(!state.equals("ticked")){
        wait();
      }
    } catch (InterruptedException exc){
      System.out.println("Thread interrupted");
    }
  }
}

class AnotherThread implements Runnable{
  Thread thread;
  TickTockWaitNotify ttObj;

  public AnotherThread(String name,TickTockWaitNotify ttObj) {
    thread = new Thread(this, name);
    this.ttObj = ttObj;
    thread.start();
  }

  @Override
  public void run() {
    if (thread.getName().compareTo("Tick") == 0){
      for(int i=0;i<5;i++) ttObj.tick(true);
      ttObj.tick(false);
    } else {
      for(int i=0;i<5;i++) ttObj.tock(true);
      ttObj.tock(false);
    }
  }
}

class threadComDemo{
  public static void main(String[] args){
    TickTockWaitNotify ttObj = new TickTockWaitNotify();
    AnotherThread t1 = new AnotherThread("Tick",ttObj);
    AnotherThread t2 = new AnotherThread("Tock",ttObj);
    try {
      t1.thread.join();
      t2.thread.join();
    } catch (InterruptedException e) {
      System.out.println("interrupted");
    }
  }
}
