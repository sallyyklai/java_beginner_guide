public class Continue {
    public static void main(String[] args) {
//        int i = 1;
        for (int i = 0; i <= 50; i++) {
//            i++;
            if((i % 2) != 0) continue; // iterate: go back to start of next iteration
            System.out.println(i);
        }
    }
}
