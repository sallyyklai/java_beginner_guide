public class OverloadingVarArgs {
//    public static void vaTest(int ... v){
////      argument with variable length must be declared last and there could be only one such arg with variable length
//        System.out.println("From the first vatest");
//        System.out.println("Number of variable args in: " + v.length);
//        System.out.println("Contents: ");
//        for(int i = 0; i < v.length; i++)
//            System.out.println(" arg " + i + ": " + v[i]);
//        System.out.println();
//    }
//    public static void vaTest(int a, int b, int ... v){
//        System.out.println("From the second vatest");
//        System.out.println("first input int: " + a);
//        System.out.println("second input int: " + b);
//        System.out.println("Number of variable args in: " + v.length);
//        System.out.println("Contents: ");
//        for(int i = 0; i < v.length; i++)
//            System.out.println(" arg " + i + ": " + v[i]);
//        System.out.println();
//    }
//    public static void main(String[] args){
//        vaTest(1,2); // so this kind of overloading does not work
//        vaTest(1,2,3);
//        vaTest(2,5,8,9);
//    }
}
