public class QFullEx extends Exception {
  int size;

  public QFullEx(int s) {
    size = s;
  }

  public String toString() {
    return "\nQueue is full, max size is: " + size;
  }
}
