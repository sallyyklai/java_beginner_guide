abstract class Shape{
    private double width;
    private double height;
    private String name;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Shape(double width, double height, String name) {
        this.width = width;
        this.height = height;
        this.name = name;
    }
    abstract double area();
}
class Triangle extends Shape {

    public Triangle(double width, double height, String name) {
        super(width, height, name);
    }

    @Override
    double area() {
        return (getHeight() * getWidth())/2.0;
    }
    @Override
    public String toString() {
        return "This is a triangle with name: " + getName() + " height: " + getHeight() + " width: " + getWidth();
    }
}
public class AbstractDemo {
    public static void main(String[] args){
        Shape shapes[] = new Shape[2];
        shapes[0] = new Triangle(2,2,"tri1");
        shapes[1] = new Triangle(2,3,"tri2");
//        shapes[2] = new Shape(2,2);
        for (Shape x:shapes){
//            System.out.println("object is " +
//                    x.getName());
//            System.out.println("Area is " + x.area());
            System.out.println(x);
        }
    }
}
