class Truck extends Vehicle {
    private int cargocap;

    public Truck(int passengers, int fuelcap, int mpg, int cargocap) {
        super(passengers, fuelcap, mpg);
        this.cargocap = cargocap;
    }

    public int getCargocap() {
        return cargocap;
    }

    public void setCargocap(int cargocap) {
        this.cargocap = cargocap;
    }
}

public class TruckDemo {
    public static void main(String args[]) {
        Truck semiTruck = new Truck(2,200,7,44000);
        Truck pickUpTruck = new Truck(3,28,15,2000);
        double gallons;
        int dist = 252;
        gallons = semiTruck.fuelneeded(dist);
        System.out.println("Semi can carry " + semiTruck.getCargocap() +
                " pounds.");
        System.out.println("To go " + dist + " miles semi needs " +
                gallons + " gallons of fuel.\n");
    }
}
